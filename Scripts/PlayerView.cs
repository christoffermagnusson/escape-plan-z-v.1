﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerView : MonoBehaviour {

    private Camera playerView;


    public GameObject gunPrefab;
    public float yOffsetGun;
    public float zOffsetGun;
    public float xOffsetGun;
    private Gun gunScript;
    private GameObject gunObj;

    private float normalX;
    private float normalY;
    private float normalZ;


    private float defaultFOV;
    private bool zoomedIn = false;
	
	void Start () {
        playerView = GetComponent<Camera>();
        defaultFOV = playerView.fieldOfView;
        normalX = transform.position.x;
        normalY = transform.position.y;
        normalZ = transform.position.z;
	}
	
	
	void Update () {
        if (Input.GetButtonDown("Zoom") && zoomedIn==false)
        {
            playerView.fieldOfView = defaultFOV * 1.5f;
            zoomedIn = true;
            Debug.Log(zoomedIn);
        }else if(Input.GetButtonDown("Zoom") && zoomedIn == true)
        {
            playerView.fieldOfView = defaultFOV;
            zoomedIn = false;
            Debug.Log(zoomedIn);
        }

        
        

}

    public void GunPickup()
    {
        float xOffset = (transform.position.x + xOffsetGun);
        float yOffset = (transform.position.y + yOffsetGun); // set from editor
        float zOffset = (transform.position.z + zOffsetGun);
        Vector3 startPos = new Vector3(xOffset, yOffset, zOffset);
        gunObj = (GameObject) Instantiate(gunPrefab,startPos,transform.rotation);
        gunObj.transform.SetParent(transform.GetChild(0),false);
        gunScript = gunObj.GetComponent<Gun>();
        gunScript.SetPickedUp(true);
        ShowCrosshair();
        StartCoroutine(ContextHandler.GetInstance().HandleTimedMessage("Gun equipped!", 2));
    }

    private void ShowCrosshair()
    {
        Image crosshair = GameObject.Find("Crosshair").GetComponent<Image>();
        Color showCrosshairAlpha = crosshair.color;
        showCrosshairAlpha.a = 160f;
        crosshair.color = showCrosshairAlpha;
    }
}
