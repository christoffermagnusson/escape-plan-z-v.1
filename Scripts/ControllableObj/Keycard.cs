﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keycard : MonoBehaviour {

    bool isInRange = false;
    bool pickedUp = false;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !pickedUp)
        {
            ContextHandler.GetInstance().Handle(this.tag);
            isInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            ContextHandler.GetInstance().Handle("Exited");
            isInRange = false;
        }
    }

    private void Update()
    {
        if (isInRange)
        {
            if (Input.GetButtonDown("Use"))
            {
                Debug.Log("PICKED UP KEYCARD");
                pickedUp = true;
                StartCoroutine(ContextHandler.GetInstance().HandleTimedMessage("Keycard added to inventory", 1));
                StartCoroutine(WaitForMessage());
                
            }
        }
    }

    private IEnumerator WaitForMessage()
    {
        yield return new WaitForSeconds(1);
        Inventory.getInstance().AddItemToInventory(Inventory.InventoryItem.KEYCARD, gameObject, Inventory.DESTROY_GAMEOBJECT);

    }
}
