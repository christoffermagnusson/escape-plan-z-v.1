﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : MonoBehaviour {


    private Animator animator;

    private bool bridgeOpen = false;
    private bool bridgeControllable = false;


    private void Start()
    {
        animator = GetComponent<Animator>();
       
        
    }

    private void Update()
    {
        if (bridgeControllable == true)
        {
            HandleBridge();
        }
        
    }

    public void SetBridgeControllable(bool bridgeControllable)
    {
        this.bridgeControllable = bridgeControllable;
    }


    /**
     * If the player is inside of the BridgeControl's collider
     * then execute this code.
     * 
     * */
    private void HandleBridge()
    {
        if (Input.GetButtonDown("Use") && bridgeOpen == false)
        {
            animator.SetTrigger("LowerBridge");
            Debug.Log("Lowering the bridge");
            bridgeOpen = true;
        }
        else if (Input.GetButtonDown("Use") && bridgeOpen == true)
        {
            animator.SetTrigger("RaiseBridge");
            Debug.Log("Raising the bridge");
            bridgeOpen = false;
        }
    }
}
