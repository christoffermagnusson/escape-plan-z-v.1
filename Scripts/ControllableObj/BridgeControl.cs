﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeControl : MonoBehaviour {

     // should also control if the player has the key to open the bridge
    private Bridge bridge;

    private void Start()
    {
        bridge = transform.parent.GetComponentInChildren<Bridge>();       
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log("Colliding with "+other.gameObject);
        if (Inventory.getInstance().InventoryCheck(Inventory.InventoryItem.KEYCARD)) // controls if the player has the keycard
        {
            ContextHandler.GetInstance().Handle(gameObject.tag);
            bridge.SetBridgeControllable(true);
        }
        else
        {
            ContextHandler.GetInstance().HandleConditionalMessage("No keycard in inventory");
        }
    }

    private void OnTriggerExit()
    {
        ContextHandler.GetInstance().Handle("Exited");
        bridge.SetBridgeControllable(false);
    }


    

}
