﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CabinDoorControl : MonoBehaviour {


    private Animator animator;
    private bool doorControllable = false;

	// Use this for initialization
	void Start () {
        this.animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (doorControllable)
        {
            if (Input.GetButtonDown("Use"))
            {
                animator.SetTrigger("OpenDoor");
            }
        }
	}

    private void OnTriggerStay(Collider other)
    {
        Debug.Log(other.gameObject + " entering trigger box");
        ContextHandler.GetInstance().Handle(gameObject.tag);
        SetDoorControllable(true);
    }

    private void OnTriggerExit(Collider other)
    {
        SetDoorControllable(false);
        ContextHandler.GetInstance().Handle("Exited");
    }

    private void SetDoorControllable(bool doorControllable)
    {
        this.doorControllable = doorControllable;
    }
}
