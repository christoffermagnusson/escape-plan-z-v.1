﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helicopter : MonoBehaviour {


    private AudioSource audioSource;
    private Animator animator;

    public float flySpeed;

    public enum State
    {
        CALLED,
        FLYING,
        LANDING,
        DEFAULT
    }

    private State currentState;

    private Transform heliPad;
	
	void Start () {
        audioSource = GetComponent<AudioSource>();
        MasterVolumeHandler.getInstance().AddAudioSource(this.gameObject, audioSource);
        animator = GetComponent<Animator>();
        currentState = State.DEFAULT;
        heliPad = GameObject.Find("HeliPad").transform;
        
	}
	
	
	void Update () {
        HandleState(currentState);
	}

    private void HandleState(State currentState)
    {
        switch (currentState)
        {
            case State.CALLED:
                audioSource.clip = SoundFX.GetClip("heli_fly");
                audioSource.loop = true;
                audioSource.Play();
                SetState(State.FLYING);
                animator.SetBool("Flying", true);
                break;
            case State.FLYING:
                transform.position = Vector3.MoveTowards(transform.position, heliPad.position, Time.deltaTime * flySpeed);
                break;
            case State.LANDING:
                // HANDLE LANDING
                break;

        }
    }

    public void SetState(State newState)
    {
        currentState = newState;
    }

    public void OnHelicopterCalled()
    {
        SetState(State.CALLED);

    }
}
