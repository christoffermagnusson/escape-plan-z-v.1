﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : MonoBehaviour {


    public GameObject listener;
    
    

    private Helicopter heli;
    private AudioSource audioSource;
    private bool hasCalledHeli = false;


	
	void Start () {
        audioSource = GetComponent<AudioSource>();
        heli = GameObject.FindObjectOfType<Helicopter>();
	}
	
	
	void Update () {
        this.transform.position = listener.transform.position; // follows the player
                
    }

   

    

    public void CallHelicopter()
    {
        audioSource.clip = SoundFX.GetClip("call_heli_1");
        audioSource.Play();
        heli.OnHelicopterCalled();
    }
}
