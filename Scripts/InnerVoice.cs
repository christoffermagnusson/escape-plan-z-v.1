﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InnerVoice : MonoBehaviour {

    private AudioSource audioSource;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        MasterVolumeHandler.getInstance().AddAudioSource(this.gameObject, audioSource);
        audioSource.clip = SoundFX.GetClip("inner_voice_01");
        //audioSource.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
