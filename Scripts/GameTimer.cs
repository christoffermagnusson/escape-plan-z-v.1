﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {

    public float gameTime;

    private Text gameTimerDisplay; 

	// Use this for initialization
	void Start () {
        gameTimerDisplay = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        gameTimerDisplay.text = FormatTime(gameTime -= Time.deltaTime);
        if(gameTime < 0)
        {
            // TODO 
            // Make end game handling
            Debug.Log("Game Over!");
        }
	}

    private string FormatTime(float timeToFormat)
    {
        float minutes = Mathf.Floor(timeToFormat / 60);
        float seconds = Mathf.RoundToInt(timeToFormat % 60);
        string secondsStr = seconds.ToString();
        if(seconds < 10)
        {
            secondsStr = "0"+ Mathf.RoundToInt(timeToFormat % 60).ToString();
        }
        return string.Format("{0}:{1}",minutes,secondsStr);
    }
}
