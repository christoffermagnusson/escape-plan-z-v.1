﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterVolumeHandler
{


    private static List<AudioSource> audioSources = new List<AudioSource>();
    private static MasterVolumeHandler instance;

    public static MasterVolumeHandler getInstance()
    {
        if (instance == null)
        {
            instance = new MasterVolumeHandler();
        }
        return instance;
    }

    public void AddAudioSource(GameObject obj, AudioSource source)
    {
        Debug.Log("Adding audioSource from " + obj);
        audioSources.Add(source);
    }

    public void ChangeVolume(float newVolume)
    {
        try
        {
            foreach (AudioSource source in audioSources)
            {
                source.volume = newVolume;
            }
        }
        catch (NullReferenceException)
        {
            Debug.Log("No AudioSource's has been instantiated yet");
        }

    }
}
