﻿using UnityEngine;
using System.Collections;

public class PlayerPrefsManager
{


    const string MASTER_VOLUME_KEY = "master_volume";
    const string GAMETIMER_KEY = "game_timer";
    const string LEVEL_KEY = "level_unlocked_"; // level_unlocked_1 for level 1 etc

    public static void SetMasterVolume(float volume)
    {
        if (volume >= 0f && volume <= 1f)
        {
            PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, volume);
        }
        else
        {
            Debug.LogError("Master volume out of range");
        }
    }

    public static float GetMasterVolume()
    {
        return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
    }

   

    public static void UnlockLevel(int levelIndex)
    {
        if (levelIndex <= Application.levelCount - 1)
        {
            PlayerPrefs.SetInt(LEVEL_KEY + levelIndex.ToString(), 1); // 1 means true , no access to bools
        }
        else
        {
            Debug.LogError("Level not in build order");
        }
    }

    public static bool IsLevelUnlocked(int levelIndex)
    {
        int levelValue = PlayerPrefs.GetInt(LEVEL_KEY + levelIndex.ToString());
        bool isLevelUnlocked = (levelValue == 1);

        if (levelIndex <= Application.levelCount - 1)
        {
            return isLevelUnlocked;
        }
        else
        {
            Debug.LogError("Trying to query non-existant level");
            return false;
        }

    }
}
