﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Class to handle incoming interactions by the player and the environment.
 * Sending messages upwards to display different texts in the UI
 * depending on which interaction type is implied
 * 
 * Could be expanded to handling events such as communicating with inventory 
 * and such
 * */

public class ContextHandler  {

    private static ContextHandler instance;

    public static ContextHandler GetInstance()
    {
        if (instance == null)
        {
            instance = new ContextHandler();
        }
        return instance;
    }

    // generic handling if no conditions
    public void Handle(string tag)
    {
        switch (tag)
        {
            case "ContextUse":
                ContextDisplay.Display("Press E key to use");
                break;
            case "ContextOpen":
                ContextDisplay.Display("Press E key to open");
                break;
            case "ContextPickUp":
                ContextDisplay.Display("Press E key to pick up item");
                break;
            case "Exited":
                ContextDisplay.Clear();
                break;
        }
    }

    public void HandleConditionalMessage(string message)
    {
        ContextDisplay.Display(message);
    }

    public IEnumerator HandleTimedMessage(string message, int duration)
    {
        return ContextDisplay.DisplayOnTime(message, duration);
    }



}
