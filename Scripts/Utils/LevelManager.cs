﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class LevelManager : MonoBehaviour {


    private bool isPaused = false;
    private Image pauseImage;

    private FirstPersonController fpsController;

    public void LoadLevel(string levelName)
    {
        Debug.Log("Loading scene : " + levelName);
        SceneManager.LoadScene(levelName);
    }

    public void QuitGame()
    {
        // TODO handling exit conditions
    }

    private void Start()
    {
        pauseImage = GameObject.Find("PauseImage").GetComponent<Image>();
        fpsController = GameObject.FindObjectOfType<Player>().GetComponent<FirstPersonController>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && !isPaused)
        {
            Time.timeScale = 0.0f;
            isPaused = true;
            Color pauseAlpha = pauseImage.color;
            pauseAlpha.a = 255f;
            pauseImage.color = pauseAlpha;
            ContextHandler.GetInstance().HandleConditionalMessage("Game Paused!");
            fpsController.enabled = false;
        }else if(Input.GetKeyDown(KeyCode.P) && isPaused)
        {
            Time.timeScale = 1f;
            isPaused = false;
            Color resumeAlpha = pauseImage.color;
            resumeAlpha.a = 0f;
            pauseImage.color = resumeAlpha;
            ContextHandler.GetInstance().HandleConditionalMessage("");
            fpsController.enabled = true;
        }
    }

}
