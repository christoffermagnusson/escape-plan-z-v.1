﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class OptionsController : MonoBehaviour {

	public Slider volumeSlider;

    private MasterVolumeHandler volumeHandler;
	
	

	// Use this for initialization
	void Start () {
		
		
		volumeSlider.value = PlayerPrefsManager.GetMasterVolume();
        volumeSlider.onValueChanged.AddListener(delegate { ChangeVolume(volumeSlider.value); }); // reacts to changes on the slider and delegates down to make appropriate calls
        volumeHandler = MasterVolumeHandler.getInstance();
		
	}

    

    

    private void ChangeVolume(float newVolume)
    {
        volumeHandler.ChangeVolume(newVolume);
    }


    public void SaveAndExit(){
		PlayerPrefsManager.SetMasterVolume(volumeSlider.value);
	}
	
	public void SetDefaults(){
		volumeSlider.value = 0.7f;
	}
}
