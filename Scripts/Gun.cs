﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    PlayerView playerView;
    BoxCollider boxCollider;
    Animator animator;
    AudioSource audio;
    public GameObject bulletHolePrefab;
    bool inRangeForPickup = false;
    bool pickedUp = false;

	// Use this for initialization
	void Start () {
        boxCollider = (BoxCollider) GetComponent<BoxCollider>();
        animator = GetComponent<Animator>();
        if (animator == null)
        {
            Debug.Log("Found no animator");
        }
        audio = GetComponent<AudioSource>();
        playerView = GameObject.FindObjectOfType<PlayerView>();
        Debug.Log("Initiating components");
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Use") && inRangeForPickup) {
            EquipGun();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            FireGun();
        }
        

    }


    private void FireGun()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            Debug.Log("Shot " + hit.collider.tag);
            Debug.DrawLine(ray.origin, hit.point);
            audio.Play();
            animator.SetTrigger("FireTrigger");
            if (hit.collider.tag != "Enemy")
            {
                Quaternion surfaceRotation = Quaternion.FromToRotation(Vector3.forward, hit.normal);
                GameObject bulletHole = Instantiate(bulletHolePrefab, hit.point, surfaceRotation);
                audio.Play();
            }
            else
            {
                // MAKE EM BLEED!
            }
        }
    }

    private void EquipGun()
    {
        playerView.GunPickup();
        Inventory.getInstance().AddItemToInventory(Inventory.InventoryItem.GUN, gameObject, Inventory.DESTROY_GAMEOBJECT);
    }

    //Pick up methods
    // For some reason I have to re-instantiate the collider and animator
    public void SetPickedUp(bool isPickedUp)
    {
        boxCollider = GetComponent<BoxCollider>();
        animator = GetComponent<Animator>();
        this.pickedUp = isPickedUp;
        DeactivateCollider();
        animator.SetTrigger("IdleTrigger");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !pickedUp)
        {
            ContextHandler.GetInstance().Handle(gameObject.tag);
            inRangeForPickup = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            ContextHandler.GetInstance().Handle("Exited");
            inRangeForPickup = false;
        }
    }

  

    public void DeactivateCollider()
    {
        try
        {
            (boxCollider as BoxCollider).size = new Vector3(0, 0, 0);
            Debug.Log("Deactivating BoxCollider");
        }
        catch (System.NullReferenceException)
        {
            Debug.Log("BoxCollider is NULL for some reason");
        }
        
    }
    public void ActivateCollider()
    {
        boxCollider.enabled = true;
    }
}
