﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryDisplay : MonoBehaviour {

    private static Image[] inventoryItems = new Image[3];
    private static int nextEmptySlot = 0;
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            inventoryItems[i] = transform.GetChild(i).GetComponent<Image>();
        }
    }

    private void Update()
    {
        Debug.Log(inventoryItems[nextEmptySlot].sprite);
    }

    public static void UpdateDisplay(Inventory.InventoryItem itemToDisplay)
    {
        switch (itemToDisplay)
        {
            case Inventory.InventoryItem.KEYCARD:
                inventoryItems[nextEmptySlot].sprite = Resources.Load<Sprite>("UI/64 flat icons/png/64px/Inventory_Key");
                nextEmptySlot++;
                break;
            case Inventory.InventoryItem.GUN:
                inventoryItems[nextEmptySlot].sprite = Resources.Load<Sprite>("UI/64 flat icons/png/64px/Weapons_Gun");
                nextEmptySlot++;
                break;
        }
    }
	
	

}
