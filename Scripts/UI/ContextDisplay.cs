﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContextDisplay : MonoBehaviour {


    private static Text display;

	void Start () {
        display = GetComponent<Text>();
	}

    public static void Display(string textToDisplay)
    {
        display.text = textToDisplay;
    }

    public static IEnumerator DisplayOnTime(string textToDisplay, int sec)
    {
        
        Debug.Log("DISPLAYING for " + sec + " seconds");
        display.text = textToDisplay;
        yield return new WaitForSeconds(sec);
        display.text = "";


    }

    public static void Clear()
    {
        display.text = "";
    }
	
	
}
