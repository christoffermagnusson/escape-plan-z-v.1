﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory {

    

    private static Inventory instance;
    private Inventory() { }
    public static Inventory getInstance()
    {
        if (instance == null)
        {
            instance = new Inventory();
        }
        return instance;
    }


    public enum InventoryItem
    {
        KEYCARD,
        GUN,
        FLAREGUN
    }
    // constants to either destroy or keep gameobjects upon inventory insertions
    public const int DESTROY_GAMEOBJECT = 0;
    public const int KEEP_GAMEOBJECT = 1;

    // Simple inventory, containing representations of Items to be checked against by the player when needed
    private Dictionary<InventoryItem, bool> inventory = new Dictionary<InventoryItem, bool>();

    /**
     * Adds an item to the inventory. 
     * @param
     * 1. item: Reference to an enum that represents the item
     * 2. gameObject: GameObject referencing the object picked up.
     *    An object could be destroyed if neccessary or handled in other ways.
     * 3. action: Action to take with the passed gameObject (either KEEP or DESTROY)
     * 
     * */
	public void AddItemToInventory(InventoryItem item, GameObject passedGameObject, int action)
    {
        Debug.Log(item + " added to inventory");
        inventory.Add(item, true);
        InventoryDisplay.UpdateDisplay(item);
        if (action == DESTROY_GAMEOBJECT)
        {
            Debug.Log("Destroying " + item);
            GameObject.Destroy(passedGameObject);
        }
    } 

    public bool InventoryCheck(InventoryItem item)
    {
        if(inventory.ContainsKey(item) && inventory[item]==true)
        {
            Debug.Log(item + " found in inventory");
            return true;
        }
        return false;
    }

    public void ClearInventory()
    {
        inventory.Clear();
    }
}
