﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour {

    // MOVE SPAWNING TO OWN SCRIPT
    public bool respawn = false;
    public Transform spawnPointParent;
    public int gameTimeInSeconds;
    private Transform[] spawnPoints;

    private bool hasCalledHeli = false;

    private Radio radio;

    // Inventory
    

    private void Start()
    {
        spawnPoints =  spawnPointParent.GetComponentsInChildren<Transform>();
        Debug.Log(spawnPoints.Length);
        radio = GameObject.FindObjectOfType<Radio>();
        
    }

    // Update is called once per frame
    void Update () {
        if (respawn == true)
        {
            int randSpawn = RandomPickSpawnPoint();
            Respawn(randSpawn);
            Debug.Log("Respawning on spawn point : " + randSpawn);

            respawn = false;
        }
        if (Input.GetButtonDown("HeliKey") && !hasCalledHeli)
        {
            Invoke("CallHelicopter",1f);  
        }
       


    }

    private void CallHelicopter()
    {
        if (gameTimeInSeconds > Time.realtimeSinceStartup)
        {
            StartCoroutine(ContextDisplay.DisplayOnTime("Wait for the timer to finish before calling heli", 3));

        }
        else
        {
            StartCoroutine(ContextDisplay.DisplayOnTime("Helicopter has been called! Make it to the pickup zone!", 5));
            radio.CallHelicopter();
            hasCalledHeli = true;
        }
    }

    private int RandomPickSpawnPoint()
    {
        int rand = UnityEngine.Random.Range(1, spawnPoints.Length);  // element Zero is parent position

        return rand;
    }

    private void Respawn(int position)
    {
        this.transform.position = spawnPoints[position].transform.position;
    }
}
